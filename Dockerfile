ARG DEBIAN_VERSION
ARG PYTHON_VERSION
ARG CYTHON_VERSION

FROM python:${PYTHON_VERSION}-slim-${DEBIAN_VERSION} AS builder

ARG CYTHON_VERSION

COPY requirements.txt .
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        build-essential && \
    python3 -m pip install -U --no-cache-dir \
        pip \
        setuptools \
        wheel && \
    python3 -m pip wheel -w /wheelhouse --no-cache-dir -r requirements.txt \
        Cython==${CYTHON_VERSION}


FROM python:${PYTHON_VERSION}-slim-${DEBIAN_VERSION}

ARG CYTHON_VERSION

COPY requirements.txt .
COPY --from=builder /wheelhouse /wheelhouse
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        build-essential \
        pkgconf && \
    python3 -m pip install -U --no-cache-dir \
        pip \
        setuptools && \
    python3 -m pip install --no-index --find-links=/wheelhouse --no-cache-dir -r requirements.txt \
        Cython==${CYTHON_VERSION} && \
    rm -r requirements.txt /wheelhouse
